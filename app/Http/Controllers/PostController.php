<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

// Auth (middleware) - access the authenticated user via Auth Facades
use Illuminate\Support\Facades\Auth;

use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    // Action to return a view containing a form for blog post
    public function create()
        {
            return view('posts.create');
        }

    public function store(Request $request)
        {
            if(Auth::user()) {
                // instantiate a new Post object from the Post Model
                $post = new Post;

                // define the properties of the $post object using the received from data.
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                // get the id of the authenticated user and set it as the foreign key user_id of the new post.
                $post->user_id = (Auth::user()->id);
                // save this post object into the database.
                $post->save();

                return redirect('/posts');

            } else {
                return redirect('/login');
            }
        }
    
    // action that will return a view showing all blog posts.
    public function index()
        {
            $posts = Post::where('is_active', true)->get();

            return view('posts.index')->with('posts', $posts);
        }

    // action for showing only the posts authored by the authenticated user
    public function myPosts()
        {
            if(Auth::user()) {
                
                // $trashedposts = Post::withTrashed()->get(); 
                $posts = Auth::user()->posts;
                return view('posts.index')->with('posts', $posts);
            } else {
                return redirect('/login');
            }
        }

    // action that will return a view with random blog posts
    public function welcome()
        {
            $posts = Post::inRandomOrder()
                    ->limit(3)
                    ->get();
            return view('welcome')->with('posts', $posts);
        }

    // action that will return a view showing the specific posts using the URL parameter $id.
    public function show($id)
        {
            $post = Post::find($id);

            return view('posts.show')->with('post', $post);
        }

    // an action to deactivate a specific post by its $id
    public function archive($id)
        {
            $post = Post::find($id);
            $post->is_active = false;
            $post->save();

           return redirect('/myPosts');
        }
    
     // an action to reactivate a specific post by its $id
    public function unarchive($id)
        {
            $post = Post::find($id);
            $post->is_active = true;
            $post->save();

           return redirect('/myPosts');
        }
    // an action to inherit the posts title and content to the edit form by its $id
    public function edit($id)
        {
            $post = Post::find($id);
            return view('posts.edit')->with('post', $post);
        }
    
    // an action to update a specific post by an authenticated user
    public function update($id, Request $request)
        {
            $post = Post::find($id);
            
                $post->title = $request->input('title');
                $post->content = $request->input('content');
                $post->save();
            
            return view('posts.show')->with('post', $post);
        }

    // define action/function for deleting post
    public function delete($id)
        {   
            $post = Post::find($id);

            if(Auth::user()->id == $post->user_id) {
                $post->delete();
            }
            
            return redirect('/posts');
        }

    // define action/function for deleting post
    public function restore($id) 
        {
            $post = Post::withTrashed()->find($id);

            
            if(Auth::user()->id == $post->user_id) {
                $post->restore();
            }
            
            return redirect('/posts');
        }

    
    public function like($id) 
        {
            $post = Post::find($id);
            $user_id = Auth::user()->id;

            // if an authenticated user is not the post author
            if($post->user_id !== $user_id) {
                // this checks if a post like has been made by a certain user before.
                if($post->likes->contains('user_id', $user_id)) {
                    // delete the like made by this user to unlike this post.
                    PostLike::where('post_id', $post->$id)->where('user_id', $user_id)->delete();
                } else {
                    // create a new like record to like a specific post
                    // instantiate a new PostLike object from the PostLike Model
                    $postlike = new Postlike;

                    // define the properties of the $postlike object
                    $postlike->post_id = $post->id;
                    $postlike->user_id = $user_id;

                    // save this $postlike object into db
                    $postlike->save();
                }

                // redirect the user back to the same post
                return redirect("/posts/$id");
            }
        }

    public function comment($id, Request $request) 
        {
            $post = Post::find($id);
            $user_id = Auth::user()->id;

            if($user_id) {
                // instantiate a new Comment object
                $postcomment = new PostComment;

                // define the properties of the $postcomment object
                $postcomment->content = $request->input('content');
                $postcomment->post_id = $post->id;
                $postcomment->user_id = $user_id;
                // save this postcomment object into the database.
                $postcomment->save();

                // redirect the user back to the same post
                return redirect("/posts/$id");
            
            }
        }

            
    // action that will return a view showing all blog posts.
    public function showcomments($id)
        {   
            $post = Post::find($id);
            $postcomments= PostComment::all();
            

            return view('posts.show')->with('postcomments', $postcomments, 'post', $post);
        }






}
