<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'posts';

    // define that the post belongs to a specific user.
    public function user() 
        {
            return $this->belongsTo('App\Models\User');
        }

    public function likes() 
        {
            return $this->hasMany('App\Models\PostLike');
        }

    public function comments() 
        {
            return $this->hasMany('App\Models\PostComment');
        }
}
