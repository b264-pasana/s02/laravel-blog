<?php

use Illuminate\Support\Facades\Route;

// Link to the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', [PostController::class, 'welcome']);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//define a route wherein a view to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein the form data will be sent via POST method to the posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);

// define a route that will retuurn a view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route where in a view showing a specific post with matching URL parameter ID will be returnedto the user
Route::get('/posts/{id}', [PostController::class, 'show']);

// define a route that will deactivate a post of the matching URL parameter ID.
Route::get('/posts/{id}/archive', [PostController::class, 'archive']);

// define a route that will reactivate a post of the matching URL parameter ID.
Route::get('/posts/{id}/unarchive', [PostController::class, 'unarchive']);

// define a route for viewing the edit post form - S)3 Activity Solution
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

// define a route that will overwrite an existing post with matching URL parameter $id via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post of a matching URL parameter $id via DELETE method
Route::delete('/posts/{id}', [PostController::class, 'delete']);

// define a route that will restore a deleted post of a matching URL parameter $id
Route::get('/posts/{id}/restore', [PostController::class, 'restore']);

// define a route that will allow users to like posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);

// define a route that will allow users to comment posts
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);

// // define a route that will return a view containing all comments
Route::get('/posts/{id}/comments', [PostController::class, 'showcomments']);
