<form method="POST" action="/posts/{{$post->id}}/comment">

    @csrf

    <div class="modal fade" id="post-comment" tabindex="-1" role="dialog" aria-labelledby="post-comment-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">

            <div class="modal-header">
              <h5 class="modal-title" id="post-comment-label">New Comment</h5>
              <button type="button" class="btn btn-light" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <form>
                <div class="form-group">
                    <textarea class="form-control" id="content" name="content" rows="8"></textarea>
                </div>
              </form>
            </div>

            <div class="modal-footer">
              <button type="submit" class="btn btn-primary">Publish</button>
            </div>

          </div>
        </div>
      </div>

</form>





  {{-- <x-modal id="post-comment" title="Comment" :centered="true">
    <x-slot name="body">
      Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.
    </x-slot>
    <x-slot name="footer">
      <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      <button type="button" class="btn btn-primary">Save changes</button>
    </x-slot>
  </x-modal> --}}