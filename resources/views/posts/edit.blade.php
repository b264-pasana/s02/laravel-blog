@extends('layouts.app')

@section('content') 
    @if(Auth::user()->id == $post->user_id)
        <form action="/posts/{{$post->id}}" method="POST">
            @method('PUT')
            @csrf

            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" id="title" value="{{$post->title}}">
            </div>

            <div class="form-group">
                <label for="content">Content:</label>
                <textarea class="form-control" id="content" name="content" rows="3" >{{$post->content}}</textarea>
            </div>

            
            <div class="mt-2">
                <button type="submit" class="btn btn-primary">Update Post</button>
            </div>

        </form>
    @else
        <h1>You do not have access on this page.</h1>
    @endif

@endsection