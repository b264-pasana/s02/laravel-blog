@if(count($replies) > 0) 
    @foreach ($replies as $reply)
    <div class="card-body">
        <h6 class="card-title"><strong>{{$reply->user->name}}:</strong></h6>
        <p class="card-text">{{$reply->content}}</p>

        <div class="form-group">
            <input type="submit" class="btn btn-warning" value="Reply" />
        </div>
        
        @include('posts.replies', ['postcomments' => $postcomment->replies])
    </div>
    @endforeach
@else
    <div>
        <p class="card-text">No posted comments yet.</p>
    </div>
@endif