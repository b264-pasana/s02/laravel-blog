@extends('layouts.app')
@include('components.modal.post-comment')

@section('content') 

    <div class="card">
        <div class="card-body">

            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}"</p>

            @if(Auth::id() != $post->user_id)
                <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                    @method('PUT')
                    @csrf
                    @if($post->likes->contains("user_id", Auth::id()))
                        <button type="submit" class="btn btn-danger">Unlike</button>
                    @else
                        <button type="submit" class="btn btn-success">Like</button>
                    @endif
                </form>
            @endif

            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#post-comment">
                Comment
              </button>
        

            <div class="mt-3">
                <a href="/myPosts" class="card-link">View All Posts</a>
            </div>

        </div>
    </div>

    <div class="card mt-3">
        <div class="card-header">
          Comments
        </div>
        @include('posts.showcomments', ['postcomments' => $post->comments, 'post_id' => $post->id])
    </div>

@endsection