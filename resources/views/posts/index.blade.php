{{-- <?//php print_r($posts); ?> --}}

@extends('layouts.app')

@section('content') 
    
    @if(count($posts) > 0) 
        @foreach ($posts as $post)
            <div class="card text-center m-3"> 
                <div class="card-body">
                    @if($post->deleted_at)
                    <h4 class="card-title mb-3 text-danger">{{$post->title}}</a>(Deleted)</h4>
                    @else
                    <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                    @endif
                    <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                    <p class="card-subtitle mb-3 text-muted">Created at: {{$post->created_at}}</p>
                    {{-- Check if there is an authenticated user to prevent our web app from throwing an error when no user is login --}}
                    {{-- @if(Auth::id() != $post->user_id)
                        <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                            @method('PUT')
                            @csrf
                            @if($post->likes->contains("user_id", Auth::id()))
                                <button type="submit" class="btn btn-danger">Unlike</button>
                            @else
                                <button type="submit" class="btn btn-success">Like</button>
                            @endif
                        </form>
                    @endif --}}
                </div>

                @if(Auth::user())
                    @if(Auth::user()->id == $post->user_id)
                        <div>
                            <form method="POST" action="/posts/{{$post->id}}">
                                @method('DELETE')
                                @csrf

                                @if(!$post->deleted_at)
                                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary mb-3">Edit Post</a>

                                    @if($post->is_active == true)
                                        <a href="/posts/{{$post->id}}/archive" class="btn btn-secondary mb-3">Archive Post</a>
                                    @else
                                        <a href="/posts/{{$post->id}}/unarchive" class="btn btn-success mb-3">Unarchive Post</a>
                                    @endif
                                @endif

                                @if($post->deleted_at)
                                <a href="/posts/{{$post->id}}/restore" class="btn btn-warning mb-3">Restore Post</a>
                                @else
                                <button type="submit" class="btn btn-danger mb-3">Delete Post</button>
                                
                                @endif
                            </form>
                        </div>
                    @endif
                @endif
            </div>
        @endforeach

    @else 
            <div>
                <h2>There are no post to show.</h2>
                <a href="/posts/create" class="btn btn-info">Create Post</a>
            </div>
    @endif

@endsection