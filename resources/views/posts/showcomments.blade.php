@if(count($postcomments) > 0) 
    @foreach ($postcomments as $postcomment)
    <div class="card-body">
        <h6 class="card-title"><strong>{{$postcomment->user->name}}:</strong></h6>
        <p class="card-text">{{$postcomment->content}}</p>

        {{-- for next development :)) --}}
        <button type="button" class="btn btn-warning" data-bs-toggle="modal" data-bs-target="#post-reply">
            Reply
          </button>
        
        {{-- @include('posts.replies', ['postcomments' => $postcomment->replies]) --}}
    </div>
    @endforeach
@else
    <div>
        <p class="card-text">No posted comments yet.</p>
    </div>
@endif